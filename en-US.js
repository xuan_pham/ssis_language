export default {
  "Nhập" : "Import" ,
  "Thêm mới" : "Add New" ,
  "Sắp xếp" : "Sort" ,
  "Ngôn ngữ" : "Language" ,
  "Đổi mật khẩu" : "Change Password" ,
  "Đăng xuất" : "Log Out" ,
  "Cài đặt giao diện" : "Settings" ,
  "Mật khẩu cũ" : "Old Password" ,
  "Mật khẩu mới" : "New Password" ,
  "Xác nhận mật khẩu" : "Confirm Password" ,
  "Lưu lại" : "Save" ,
  "Xác nhận mật khẩu không đúng" : "Confirm Password Not Matched" ,
  "Trạng thái" : "Status" ,
  "tên đăng nhập, sđt" : "Username, Phone Number" ,
  "Tài khoản" : "Account" ,
  "Tên đầy đủ" : "Full Name" ,
  "Số điện thoại" : "Phone Number" ,
  "Nhóm người dùng" : "Role" ,
  "Đối tượng áp dụng" : "Applied Objects" ,
  "Chỉnh sửa" : "Edit" ,
  "Khóa" : "Inactivate" ,
  "Mở khóa" : "Activate" ,
  "Thêm mới / Cập nhật thông tin tài khoản" : "Add New / Update" ,
  "Mật khẩu" : "Password" ,
  "Tên hiển thị" : "Display Name" ,
  "Nhập để tìm kiếm" : "Input Text to Search" ,
  "Lớp" : "Class" ,
  "Khối" : "Grade" ,
  "Khôi phục mật khẩu thành công" : "Passwords Reset Successfully" ,
  "Có lỗi xãy ra" : "Internal Errors" ,
  "Lưu thành công" : "Save Successfully" ,
  "Lưu không thành công" : "Save Unsuccessfully" ,
};
